package Part1;
import java.util.ArrayList;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import Core.Window;

public class Joc1 {

	//declaracio de field
    static Field f = new Field();
    //declracio de window
    static Window w = new Window(f);	
	
    
    
	public static void main(String[] args) throws InterruptedException {
	
		ArrayList<Sprite> sprites = new ArrayList<Sprite>();
	
		Sprite roca = new Sprite("Roca", 50, 50, 100, 150, 0, "resources/rock1.png", f );
		sprites.add(roca);
		
		
		Roca r1 = new Roca("r1", 100, 0, 150, 50, 0, "resources/rock1.png", f);
		sprites.add(r1);
		Roca r2 = new Roca("r1", 400, 0, 450, 50, 0, "resources/rock1.png", f, 10);
		sprites.add(r2);
		Roca r3 = new Roca(200, 0, 250, 50, f, 20);
		sprites.add(r3);
		Roca r4 = new Roca(300, 0, f, 50);
		sprites.add(r4);
		Roca r5 = new Roca();
		sprites.add(r5);
		
		System.out.println(r5.comptador);
		
	boolean sortir = false;
////////
    while (!sortir) {
    	
        f.draw(sprites);
        Thread.sleep(30);
        
    }
////////
	}

	
}
