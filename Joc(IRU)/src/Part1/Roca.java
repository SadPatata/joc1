package Part1;

import java.awt.Color;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import Core.Window;

public class Roca extends Sprite {
	// VARIABLES
	public static int comptador=0;
	protected int accionsDisponibles;

	// CONSTRUCTORES
	public Roca(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name+comptador, x1, y1, x2, y2, angle, path, f);
		comptador++;
	}
	
	public Roca(String name, int x1, int y1, int x2, int y2, double angle, Color color, Field f) {
		super(name+comptador, x1, y1, x2, y2, angle, color, f);
		comptador++;
	}

	public Roca(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, int accions) {
		super(name+comptador, x1, y1, x2, y2, angle, path, f);
		this.accionsDisponibles = accions;
		comptador++;
	}

	public Roca(int x1, int y1, int x2, int y2, Field f, int accions) {
		super("roca"+comptador, x1, y1, x2, y2, 0, "resources/rock1.png", f);
		this.accionsDisponibles = accions;
		comptador++;
	}
	
	public Roca(int x1, int y1, Field f, int tamany) {
		super("roca"+comptador, x1, y1, (x1+tamany), (y1 + tamany), 0, "resources/rock1.png", f);
		this.accionsDisponibles = 50;
		comptador++;
	}
	
	public Roca() {
		super("roca"+comptador, 0, 0, 50, 50, 0, "resources/rock1.png", Joc1.f);
		this.accionsDisponibles = 50;
		comptador++;
	}

}
