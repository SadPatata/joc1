package Part2;
import java.awt.Color;
import java.util.Set;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class Personatge extends PhysicBody{

	
	public boolean aterra;	
	static int doublejump;
	//CONSTRUCTORES

	public Personatge(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.setConstantForce(0, 0.2);
	}

	//FUNCIONES
	@Override
	public void onCollisionEnter(Sprite sprite) {
		
		if(sprite instanceof Part1.Roca && this.y2 <= sprite.y2) {
			aterra = true;
			doublejump = 2;
			flippedY = false;
		}
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		flippedY=true;
		if(sprite instanceof Part1.Roca) {
			aterra = false;
			
		}
	}
	
	
public void movimentp(Set<Character> key){
		
		if(key.contains('d') || key.contains('D')){
			this.setVelocity(3, this.velocity[1]);
			flippedX = true;
		}
		else if(key.contains('a')|| key.contains('A')){
			this.setVelocity(-3, this.velocity[1]);
			flippedX = false;
		}else if(aterra) {
			this.setVelocity(0, this.velocity[1]);
		}
				
	}

public void saltar() {
	
	if(aterra == true || doublejump > 0) {
				this.setForce(0, 0);
				this.addForce(0, -2.2);
			
				doublejump--;
			}
}

}
