package Part2;
import Core.Field;
import Core.Window;
public class Control {

    static Field f = new Field();  
    static Window w = new Window(f);	
	
	public static void main(String[] args) throws InterruptedException {
		
		
		
		
		RocaControlable RC = new RocaControlable("Boulder", 50, 50, 100, 100, 0, "resources/rock1.png", f, 50000);

		boolean sortir = false;
	////////
	    while (!sortir) {
	    	Input(RC);
	        f.draw();
	        Thread.sleep(30);
	     
	        
	    
	        
	    }
	////////
		
	}

	public static void Input(RocaControlable ro) {
		
		if(w.getPressedKeys().contains('w')) {
		    ro.moviment(Input.AMUNT);
		}else if(w.getPressedKeys().contains('s')) {
			ro.moviment(Input.AVALL);
		}else if(w.getPressedKeys().contains('a')) {
			ro.moviment(Input.ESQUERRA);
		}else if(w.getPressedKeys().contains('d')) {
			ro.moviment(Input.DRETA);
		}
	}
	
}
