package Part2;

import java.util.Set;

import javax.xml.stream.events.Characters;

import Core.Field;
import Core.Window;
import Part1.Roca;

public class PerMain {

	static Field f = new Field();
	static Window w = new Window(f);

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		Personatge POG = new Personatge("Lunk", 100, 100, 150, 150, 0, "resources/Link1.gif", f);

		Roca r = new Roca("Floor", 0, 900, 1000, 950, 0, "resources/rock1.png", f);
		Roca r2 = new Roca("Cealing", 0, 0, 1000, 25, 0, "resources/rock1.png", f);
		Roca plat = new Roca("Cealing", 700, 700, 825, 710, 0, "resources/rock1.png", f);

		boolean sortir = false;
		////////
		while (!sortir) {
			
			outofbounds(POG);		
			Set<Character> keys = Input();
			move(POG, keys);
			f.draw();
			Thread.sleep(30);
			
		}
		////////

	}

	private static void outofbounds(Personatge POG) {
		if (POG.y1 > 1100) {

				POG.y1 = 50;
				POG.y2 = 100;
			}
		
	}

	public static void move(Personatge per ,Set<Character> keys) {
		
		per.movimentp(keys);
		if(w.getKeysDown().contains(' ')) {
			per.saltar();
		}
		
	}
	

	
	
	
	public static Set<Character> Input() {

		return w.getPressedKeys();

	}

}