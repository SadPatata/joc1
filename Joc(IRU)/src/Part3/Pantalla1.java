package Part3;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Set;
import Part1.Roca;
import Core.Field;
import Core.Sprite;
import Core.Window;

public class Pantalla1 {
	
	static Field f = new Field();
	static Window w = new Window(f);
	
	public static void Start() throws InterruptedException {
		
		
		Sprite FONDO = new Sprite("Fondo", 0, 0, 2000, 1200, 0, "resources/fondo.gif", f);
		FONDO.solid=false;
		FONDO.unscrollable = true;
		Sprite barra = new Sprite("Energiab", 18, 93, 133, 127, 0, "resources/barra.png", f);
		barra.solid = false;
		barra.unscrollable = true;
	//	Sprite FONDO1 = new Sprite("Fondo", 3000, 0, 6000, 1100, 0, "resources/fondo.gif", f);
		//FONDO1.solid=false;
		//Sprite FONDO2 = new Sprite("Fondo", 6000, 0, 9000, 1100, 0, "resources/fondo.gif", f);
		//FONDO2.solid=false;

		
		////////////////////////////////////////////////////////////////////////////////////////
		Roca Plat1 = new Roca("Edificio1", 0, 500, 700, 1100, 0, "resources/E1.png", f);
		Roca Plat1_1 = new Roca("Edificio1_at", 0, 350, 100, 500, 0, "resources/muro.png", f);
		/////////////////////////
		Roca Plat2_11 = new Roca("Edificio2_Ini", 1000, 500, 3000, 1100, 0, "resources/E2.png", f);
		Roca Plat2_1 = new Roca("Edificio2_Plat1", 1100, 400, 1300, 410, 0, "resources/muro.png", f);
	//	Sprite Plat2_1_1 = new Sprite("Edicicio2_Plat1_Under", 1100, 410, 1300, 500, 0, Color.gray, f);
	//	Plat2_1_1.solid = false;
		Roca Plat2_2 = new Roca("Edificio2_Plat2", 1500, 400, 1700, 410, 0, "resources/muro.png", f);
	//	Sprite Plat2_2_1 = new Sprite("Edicicio2_Plat2_Under", 1500, 410, 1700, 500, 0, Color.gray, f);
	//	Plat2_2_1.solid = false;
		Roca Plat2_3 = new Roca("Edificio2_Plat3", 1900, 300, 2300, 310, 0, "resources/muro.png", f);
		Roca Plat2_4 = new Roca("Edificio2_Plat4", 2600, 300, 3000, 310, 0, "resources/muro.png", f);
		/////////////////////////
		Roca Plat3 = new Roca("Edificio3", 3300, 500, 4000, 1100, 0, "resources/E1.png", f);
		////////////////////////
		Roca Plat4 = new Roca("Edificio2_Ini", 4300, 500, 6300, 1100, 0, "resources/E2.png", f);
		Roca Plat4_1 = new Roca("Edificio4_bl", 4600, 400, 5000, 500, 0, "resources/muro.png", f);
		Roca Plat4_2 = new Roca("Edificio4_Plat2", 5200, 300, 5500, 310, 0, "resources/muro.png", f);
		Roca Plat4_3 = new Roca("Edificio4_Plat3", 5700, 300, 6000, 310, 0, "resources/muro.png", f);
		///////////////////////
		ArrayList<Enemic> LEnemics = new ArrayList<Enemic>();
		Enemic bad = new Enemic("Enemigo", 1400, 425, 1475, 500, 0, "resources/badguy.gif", f, 1, 200, 400);
		LEnemics.add(bad);
		Enemic bad1 = new Enemic("Enemigo_Primero", 2825, 425, 2900, 500, 0, "resources/badguy.gif", f, 1,true);
		LEnemics.add(bad1);
		Enemic bad2 = new Enemic("Enemigo_segundo", 2025, 175, 2100, 250, 0, "resources/badguy.gif", f, 1,false);
		LEnemics.add(bad2);
		EnemicVolador vad = new EnemicVolador("Enemigo_Volador", 3500, 200, 3575, 275, 0, "resources/enemigovolador.gif", f, 1, 200, 400);
		LEnemics.add(vad);
		Enemic bad3 = new Enemic("Enemigo_tercero", 4800, 325, 4875, 400, 0, "resources/badguy.gif", f, 1,false);
		LEnemics.add(bad3);
		Enemic bad4 = new Enemic("Enemigo_cuarto", 5300, 325, 5375, 400, 0, "resources/badguy.gif", f, 1, 100, 200);
		LEnemics.add(bad4);
		EnemicVolador vad1 = new EnemicVolador("Enemigo_Volador1", 5800, 100, 5875, 175, 0, "resources/enemigovolador.gif", f, 1, 100, 200);
		LEnemics.add(vad1);
		///////////////////////
		 Obstacle box = new Obstacle("box", 250, 400, 300, 450, 0, "resources/caja.png", f);
		 Obstacle box1 = new Obstacle("box1", 260, 300, 310, 350, 0, "resources/caja.png", f);
		 Obstacle box2 = new Obstacle("box2", 2400, 400, 2450, 450, 0, "resources/caja.png", f);
		 Obstacle box3 = new Obstacle("box3", 2460, 400, 2510, 450, 0, "resources/caja.png", f);
		 Obstacle box4 = new Obstacle("box4", 2425, 300, 2475, 350, 0, "resources/caja.png", f);
		 Obstacle box5 = new Obstacle("box5", 3600, 400, 3650, 450, 0, "resources/caja.png", f);
		 Obstacle box6 = new Obstacle("box6", 3600, 300, 3650, 350, 0, "resources/caja.png", f);
		 Obstacle box7 = new Obstacle("box7", 3660, 400, 3710, 450, 0, "resources/caja.png", f);
		 Obstacle box8 = new Obstacle("box8", 3660, 300, 3710, 350, 0, "resources/caja.png", f);
		 Obstacle box9 = new Obstacle("box9", 5800, 400, 5850, 450, 0, "resources/caja.png", f);
		 Obstacle box10 = new Obstacle("box10", 5825, 320, 5875, 370, 0, "resources/caja.png", f);
		 Obstacle box11 = new Obstacle("box11", 5860, 400, 5910, 450, 0, "resources/caja.png", f);
		//////////////////////
		Pocion pot = new Pocion("pocion", 2450, 150, 2475, 175, 0, "resources/corazon1.gif", f);
		Puerta door = new Puerta("P2", 6200, 400, 6300, 500, 0, "resources/puerta.png", f);
		//Sprite zero= new Sprite("zero", 100, 100, 110, 120, 0, Color.black, f);
		//Sprite cien= new Sprite("cien", 200, 100, 210, 120, 0, Color.black, f);
		
		Personatge POG = new Personatge("Lunk", 110, 400, 185, 475, 0, "resources/robot.gif", f);
		
	
		boolean sortir = false;

		while (!sortir) {

			
			Comportamiento.Camera(POG, f);
			Comportamiento.outofbounds(POG);
			Set<Character> keys = Comportamiento.Input(w);
			Set<Character> pe = Comportamiento.InputPew(w);
			Comportamiento.move(POG, keys, pe);
			Comportamiento.EnemicIA(LEnemics, POG);

			
			f.draw();
			Thread.sleep(30);

		}
	}
}
