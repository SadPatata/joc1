package Part3;
import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class Projectil extends PhysicBody{

	/**
	 * X1 inicial del Proyectil
	 */
	private int inix;
	/**
	 * Y1 inicial del proyectil.
	 */
	private int iniy;
	/**
	 * Da�o que causa el proyectil.
	 */
	private int dany;
	
	//CONS
	/**
	 * Constructor de proyectil.
	 * @param name
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param angle
	 * @param path
	 * @param f
	 * @param Dany
	 */
	public Projectil(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, int Dany) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.trigger = true;
		inix = this.x1;
		iniy = this.y1;
		this.dany = Dany;
	}
	
	/**
	 * Constructor por copia de proyectil.
	 * @param p
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 */
	public Projectil(Projectil p, int x1, int y1, int x2, int y2) {
		super(p.name, x1, y1, x2, y2, p.angle, p.path, p.f);
		this.trigger = true;
		inix = this.x1;
		iniy = this.y1;
		this.dany = p.dany;
	}
	
	//FUN
	/**
	 * Funcion que detecta y procesa las colisiones de Proyectil.
	 */
	@Override
	public void onCollisionEnter(Sprite sprite) {
		
		if((!(sprite instanceof Enemic) && this.name.contains("bad")) || this.name.contains("spit")) {
			System.out.println("coll with "+sprite.name);
		 System.out.println();
	        if(sprite instanceof Disparable) { 
	            Disparable d = (Disparable) sprite; 
	            d.danyar(this.dany);    
	        }  
	        if(sprite instanceof Enemic) { 
	            Enemic e = (Enemic) sprite; 
	            e.back(this.velocity[0]);  
	        }  
	        
	        
	        
	        this.delete();
		}
		
		 
	}
	/**
	 * Funcion que detecta y procesa la salida de las colision de Proyectil.
	 */
	@Override
	public void onCollisionExit(Sprite sprite) {

		
	}
	/**
	 * Funcion a la que se llama cada frame.
	 */
	@Override
	public void update() {

		if(this.x1 > inix +700 || this.x1 < inix -700 || this.y1 > this.iniy+700 || this.y1 < this.iniy-700 ){
			this.delete();
		}
		
	}
	
	

}
