package Part3;

public interface Hostil
{
	/**
	 * Funcion que comprueba si el personaje esta en rango de persecucion.
	 * @param x1
	 * @param x2
	 * @param y1
	 * @param y2
	 * @return
	 */
	public int inRangeOf(int x1, int x2, int y1, int y2);
	
	/**
	 * Funcion que comprueba si el personaje esta en rango de disparo.
	 * @param x1
	 * @param x2
	 * @param y1
	 * @param y2
	 * @return
	 */
	public int inShootRange(int x1, int x2, int y1, int y2);
	
}
