package Part3;

import Core.Field;

public class EnemicVolador extends Enemic {

	/**
	 * Constructor de enemigo.
	 * @param name
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param angle
	 * @param path
	 * @param f
	 * @param Dmg
	 * @param stac
	 */
	public EnemicVolador(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, int Dmg,
			boolean stac) {
		super(name, x1, y1, x2, y2, angle, path, f, Dmg, stac);
		this.setConstantForce(0, 0);

	}
	/**
	 * Constructor de enemigo con patrulla.
	 * @param name
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param angle
	 * @param path
	 * @param f
	 * @param Dmg
	 * @param xi
	 * @param xf
	 */
	public EnemicVolador(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, int Dmg,
			int xi, int xf) {
		super(name, x1, y1, x2, y2, angle, path, f, Dmg, xi, xf);
		this.setConstantForce(0, 0);
	}

	/**
	 * Funcion a la que se llama cada frame.
	 */
	@Override
	public void update() {

		this.cooldown++;
		
		if (this.Patrulla) {

			if (this.x1 <= this.xpati) {
				this.flippedX = false;
				this.setVelocity(7, 0);
			}

			if (this.x2 >= this.xpatf) {
				this.flippedX = true;
				this.setVelocity(-7, 0);
			}

		}
	}

	@Override
	public void danyar(int dmg) {

		this.vida -= dmg;
		if (this.vida <= 0) {
			this.delete();

			if (r.nextInt(4) == 0) {
				Pocion p = new Pocion("Loot", this.x1 + 25, this.y1 + 25, this.x1 + 50, this.y1 + 50, 0,
						"resources/corazon1.gif", f);
			}
		}
	}

	/**
	 * Funcion que controla el disparo del enemigo.
	 */
	@Override
	public void shoot() {
		if (this.vida > 0) {

			if (cooldown >= 20) {
				this.cosa = new Projectil("bad", (this.x1 + 12), this.y1 + 76, (this.x2 - 43), this.y2 + 25, 90,
						"resources/Proyectil1.png", f, DMG);
				cosa.setVelocity(0, 10);
				cooldown = 0;
			}
		}
	}

	public int inRangeOf(int x1, int x2, int y1, int y2) {
		if (this.x1 + 300 > x1 && x1 > this.x1) {
			return 1;
		}
		if (this.x1 - 300 < x1 && x1 < this.x1) {
			return 2;
		}
		return 0;
	}
	
	public int inShootRange(int x1, int x2, int y1, int y2) {
		return 1;
	}
}
