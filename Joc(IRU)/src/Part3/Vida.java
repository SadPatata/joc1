package Part3;

import java.awt.Color;

import Core.Field;
import Core.Sprite;

public class Vida extends Sprite {

	// CON
	/**
	 * Constructor de Vida, llama a super, pone solid y unscrollable.
	 * 
	 * @param name
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param angle
	 * @param path
	 * @param f
	 */
	public Vida(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.solid = false;
		this.unscrollable = true;
	}

	public Vida(String name, int x1, int y1, int x2, int y2, double angle, Color color, Field f) {
		super(name, x1, y1, x2, y2, angle, color, f);
		this.solid = false;
		this.unscrollable = true;
	}

	// FUN
	/**
	 * Funcion a la que se llama cada frame.
	 */
	@Override
	public void update() {

	}

}
