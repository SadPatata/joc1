package Part3;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class Puerta extends PhysicBody{

	/**
	 * Constructor de puerta.
	 * @param name
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param angle
	 * @param path
	 * @param f
	 */
	public Puerta(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		// TODO Auto-generated constructor stub
	}
	/**
	 * Funcion que pasa el personaje a la siguiente pantalla.
	 * @param p
	 */
	public void next(Personatge p) {

		if (this.name.equals("P2"))
			try {
				Pantalla2.P2(p);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

	}

	/////////////////////////////funciones no utilizadas.
	@Override
	public void onCollisionEnter(Sprite sprite) {
				
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

	
	
}
