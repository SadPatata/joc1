package Part3;

import java.util.ArrayList;
import java.util.Set;
import Core.Field;
import Core.Window;

public class Comportamiento {

	/**
	 * Control del movimiento de la camara
	 * @param p
	 * @param f
	 */
	public static void Camera(Personatge p, Field f) {

		if (p.x1 > 600 && p.velocity[0] > 0 && f.getScrollx()>-4400) {
			f.scroll(-8, 0);
		}
		if (f.getScrollx()<0 && p.velocity[0] < 0 && p.x1 < 5000) {
			f.scroll(8, 0);
		}
	}

	/**
	 * Determina si el enemigo devera seguir y disparar al jugador.
	 * @param L
	 * @param POG
	 */
	public static void EnemicIA(ArrayList<Enemic> L, Personatge POG) {

		
		for (int i = 0; i < L.size(); i++) {
			if(!L.get(i).siguiendo) {
			if(L.get(i).inRangeOf(POG.x1, POG.x2, POG.y1, POG.y2) == 1) {
			L.get(i).setStacionary(false);
			L.get(i).setPatrulla(false);
			L.get(i).setVelocity(7, 0);
			L.get(i).siguiendo = true;
			
			}		
		}
			if(!L.get(i).siguiendo) {
			if(L.get(i).inRangeOf(POG.x1, POG.x2, POG.y1, POG.y2) == 2) {
			L.get(i).setStacionary(false);
			L.get(i).setPatrulla(false);
			L.get(i).setVelocity(-7, 0);
			L.get(i).siguiendo = true;
			}
			}
		
			if(L.get(i).siguiendo) {
				if(L.get(i).inShootRange(POG.x1, POG.x2, POG.y1, POG.y2) == 1){
				L.get(i).shoot();
				}
			}
			
				
			if(L.get(i).inRangeOf(POG.x1, POG.x2, POG.y1, POG.y2) == 0 && L.get(i).siguiendo == true) {
				L.get(i).volver();			
				L.get(i).siguiendo = false;
				}		
		}
	}
	
	/**
	 * Comprueba si el jugador se ha caido del nivel.
	 * @param POG
	 */
	public static void outofbounds(Personatge POG) {
		if (POG.y1 > 1100) {
			POG.danyar(1);
			POG.y1 = 50;
			POG.y2 = 125;
			
		}

	}

	/**
	 * Movimiento del jugador.
	 * @param per
	 * @param keys
	 * @param pe
	 */
	public static void move(Personatge per, Set<Character> keys, Set<Character> pe) {

		per.movimentp(keys);
		if (pe.contains('w')) {
			per.saltar();
		}

	}
	
	/**
	 * Recoge las teclas mantenidas.
	 * @param w
	 * @return
	 */
	public static Set<Character> Input(Window w) {

		return w.getPressedKeys();

	}

	/**
	 * Recoge las teclas pulsadas. 
	 * @param w
	 * @return
	 */
	public static Set<Character> InputPew(Window w) {

		return w.getKeysDown();

	}

}
