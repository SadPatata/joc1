package Part3;
import java.util.Random;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class Obstacle extends PhysicBody implements Disparable{
	
	Random r = new Random();
//CON
/**		
 * Constructor de Obstacle: Llama a super y pone la ConstantForce.
 * @param name
 * @param x1
 * @param y1
 * @param x2
 * @param y2
 * @param angle
 * @param path
 * @param f
 */
public Obstacle(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.setConstantForce(0, 0.2);
	}

	//FUN	
	/**
	 * Funcion que detecta y procesa las colisiones de Obstacle.
	 */
	@Override
	public void onCollisionEnter(Sprite sprite) {

		
	}
	/**
	 * Funcion que detecta y procesa la salida de las colision de Obstacle.
	 */
	@Override
	public void onCollisionExit(Sprite sprite) {

		
	}
	/**
	 * Funcion que destruye el obstaculo.
	 */
	@Override
	public void danyar(int a) {
	
		this.delete();
		if (r.nextInt(5) == 0) {
			Pocion p = new Pocion("Loot", this.x1 + 25, this.y1 + 25, this.x1 + 50, this.y1 + 50, 0,
					"resources/corazon1.gif", f);
		}
	}

}
