package Part3;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import java.util.Random;

public class Enemic extends PhysicBody implements Disparable, Hostil {

	Random r = new Random();

	/**
	 * Vida del enemigo.
	 */
	protected int vida;
	/**
	 * Proyectil del enemigo.
	 */
	public Projectil cosa;
	/**
	 * Tiempo de espera entre disparos del enemigo.
	 */
	protected int cooldown;
	/**
	 * Da�o que hace el enemigo.
	 */
	protected int DMG;
	/**
	 * Booleano para comprovar si es un enemigo estacionario.
	 */
	protected boolean Stacionary;
	/**
	 * Booleano que comprueva si el enemigo era originalmente estacionario.
	 */
	protected boolean Stacionaryini;
	/**
	 * Tiempo de espera para girarse de los enemigos estacionarios.
	 */
	protected int turnaround;
	/**
	 * Booleano que deretmina si el enemigo esta siguiendo al jugador.
	 */
	public boolean siguiendo;
	/**
	 * Booleano que determina si es un enemigo con patrulla.
	 */
	protected boolean Patrulla;
	/**
	 * Booleano que determina si el enemigo originalmente estaba en una patrulla.
	 */
	protected boolean Patrullaini;
	/**
	 * Punto inicial de la patrulla.
	 */
	protected int xpati;
	/**
	 * Punto final de la patrulla.
	 */
	protected int xpatf;

//CON	

	/**
	 * Constructor de enemigo estacionario o no.
	 * @param name
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param angle
	 * @param path
	 * @param f
	 * @param Dmg
	 * @param stac
	 */
	public Enemic(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, int Dmg,
			boolean stac) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.vida = 4;
		this.setConstantForce(0, 0.2);
		this.Stacionary = stac;
		this.Stacionaryini = stac;
		if (!this.Stacionary) {
			this.setVelocity(-7, 0);
		}
		flippedX = true;
		this.cooldown = 0;
		this.layer = 1;
		this.ignoredLayers.add(1);
		this.DMG = Dmg;
		this.turnaround = 0;
		this.Patrulla = false;
		this.Patrullaini = false;
	}
	/**
	 * Constructor de enemigo con p�trulla.
	 * @param name
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param angle
	 * @param path
	 * @param f
	 * @param Dmg
	 * @param xi
	 * @param xf
	 */
	public Enemic(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, int Dmg, int xi,
			int xf) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.vida = 4;
		this.setConstantForce(0, 0.2);
		this.Stacionary = false;
		this.Stacionaryini = false;
		if (!this.Stacionary) {
			this.setVelocity(-7, 0);
		}
		flippedX = true;
		this.cooldown = 0;
		this.layer = 1;
		this.ignoredLayers.add(1);
		this.DMG = Dmg;
		this.turnaround = 0;
		this.Patrulla = true;
		this.Patrullaini = true;
		this.xpati = this.x1 - xi;
		this.xpatf = this.x2 + xf;
	}
//FUN	

	
	public void setStacionary(boolean stacionary) {
		Stacionary = stacionary;
	}

	public void setPatrulla(boolean pat) {
		Patrulla = pat;
	}

	/**
	 * Funcion a la que se llama cada frame.
	 */
	@Override
	public void update() {

		cooldown++;
		if (this.Stacionary) {
			this.turnaround++;
			if (this.turnaround >= 300) {
				turn();
				this.turnaround = 0;
			}
		}

	}
	/**
	 * Gira el sprite en las X.
	 */
	private void turn() {

		if (!this.flippedX) {
			this.flippedX = true;
		} else {
			this.flippedX = false;
		}

	}

	/**
	 * Funcion que detecta y procesa las colisiones mantenidas de Enemic.
	 */
	@Override
	public void onCollisionStay(Sprite sprite) {

		if (!this.Stacionary) {
			if (this.Patrulla) {

				if (this.x1 <= this.xpati) {
					this.flippedX = false;
					this.setVelocity(7, 0);
				}

				if (this.x2 >= this.xpatf) {
					this.flippedX = true;
					this.setVelocity(-7, 0);
				}

			}
			if (this.x1 - 1 < sprite.x1) {
				this.flippedX = false;
				this.setVelocity(7, 0);

			}
			if (this.x2 + 1 > sprite.x2) {
				this.flippedX = true;
				this.setVelocity(-7, 0);

			}
		} else {
			this.setVelocity(0, 0);
		}

	}

	/**
	 * Funcion que detecta y procesa las colisiones de Enemic.
	 */
	@Override
	public void onCollisionEnter(Sprite sprite) {

		if (!this.Stacionary) {

			if (sprite.y1 < this.y2) {

				if (this.flippedX) {
					this.flippedX = false;
					this.setVelocity(7, 0);
				} else {
					this.flippedX = true;
					this.setVelocity(-7, 0);
				}

			}
		}

	}

	/**
	 * Funcion que detecta y procesa la salida de las colision de Enemic.
	 */
	@Override
	public void onCollisionExit(Sprite sprite) {

	}
	public void danyar(int dmg) {

		this.vida -= dmg;
		this.setForce(0, -1);
		if (this.vida <= 0) {
			this.delete();

			if (r.nextInt(4) == 0) {
				Pocion p = new Pocion("Loot", this.x1 + 25, this.y1 + 25, this.x1 + 50, this.y1 + 50, 0,
						"resources/corazon1.gif", f);
			}
		}
	}
	/**
	 * Funcion que les hace girarse al recibir un disparo por la espalda.
	 * @param vel
	 */
	public void back(double vel) {

		if (this.flippedX && vel < 0) {
			this.flippedX = false;
			if (!this.Stacionary) {
				this.setVelocity(7, 0);
			}
		} else if (!this.flippedX && vel > 0) {
			this.flippedX = true;
			if (!this.Stacionary) {
				this.setVelocity(-7, 0);
			}
		}

	}

	/**
	 * Funcion que controla el disparo del enemigo.
	 */
	public void shoot() {

		if (this.vida > 0) {

			if (!this.flippedX && cooldown >= 50) {
				this.cosa = new Projectil("bad", (this.x1 + 76), this.y1 + 12, (this.x2 + 25), this.y2 - 43, 0,
						"resources/Proyectil1.png", f, DMG);
				cosa.setVelocity(15, 0);
				cooldown = 0;
			} else if (this.flippedX && cooldown >= 50) {

				this.cosa = new Projectil("bad", (this.x1 - 25), this.y1 + 12, (this.x2 - 76), this.y2 - 43, 0,
						"resources/Proyectil1.png", f, DMG);
				cosa.setVelocity(-15, 0);
				cooldown = 0;
				cosa.flippedX = true;

			}
		}
	}
	/**
	 * Funcion con la que los enemigos vuelven a su comportamiento natural tras seguir al jugador.
	 */
	public void volver() {

		if (this.Stacionaryini) {
			this.Stacionary = true;
		}

		if (this.Patrullaini) {
			this.Patrulla = true;
		}

	}

	public int inRangeOf(int x1, int x2, int y1, int y2) {
		if ((this.x1 + 700) > x1 && x1 > this.x1 && this.flippedX == false
				&& (y2 == this.y2 || y1 == this.y1 || y1 == this.y1+25 || y2 == this.y2+25)) {
			return 1;
		}else if ((this.x1 - 700) < x1 && x1 < this.x1 && this.flippedX == true
				&& (y2 == this.y2 || y1 == this.y1 || y1 == this.y1+25 || y2 == this.y2+25)) {
			return 2;
		}
		return 0;
	}

	public int inShootRange(int x1, int x2, int y1, int y2) {
		if(this.x1 - 400 < x1 && this.x1 > x1 ) {
			return 1;
		}else if(this.x1 + 400 > x1 && this.x1 < x1) {
			return 1;
		}
		return 0;
	}
	
	
}
