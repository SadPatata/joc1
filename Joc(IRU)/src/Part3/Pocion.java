package Part3;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class Pocion extends PhysicBody{
	/*
	 * Contructor de pocion.
	 */
	public Pocion(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.trigger = true;
	}
	
	/**
	 * Funcion que destrulle el sprite de pocion.
	 */
	public void recoger() {
		this.delete();
	}

	
	///////////////////////////////Metodos no utilizados.
	
	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}
	
}
