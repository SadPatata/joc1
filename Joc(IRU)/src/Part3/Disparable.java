package Part3;

public interface Disparable {

	/**
	 * Funcion que procesa el da�o recibido.
	 * @param a
	 */
	public abstract void danyar(int a);
}
