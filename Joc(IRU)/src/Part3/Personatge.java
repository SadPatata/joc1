package Part3;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Set;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class Personatge extends PhysicBody implements Disparable {

	/**
	 * Booleano de comprobacion de si esta tocando el suelo.
	 */
	private boolean aterra;
	/**
	 * Variable que define el numero de veces que se puede saltar.
	 */
	private int doublejump;
	/**
	 * Proyectil que disparara el personaje.
	 */
	private Projectil pew;
	/**
	 * Vida actual del personaje
	 */
	private int vida;
	/**
	 * Array que contendra los sprites representativos de la vida del personaje.
	 */
	ArrayList<Vida> vidas = new ArrayList<Vida>();
	/**
	 * Vida maxima del personaje.
	 */
	private int vidamax;
	/**
	 * Energia o municion del personaje.
	 */
	private int energia;
	/**
	 * Energia o municion maxima del personaje.
	 */
	private int energiamax;
	/**
	 * Tiempo de espera entre disparos.
	 */
	private int cooldown;
	/**
	 * Representacion grafica de la energia del personaje.
	 */
	private Vida Br_ene;
	/**
	 * Tiempo de recarga de una unidad de energia.
	 */
	private int Br_rec;
	/**
	 * Da�o del personaje.
	 */
	private int DMG;
	// CONSTRUCTORES

	/**
	 * Constructor de personaje.
	 * @param name
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param angle
	 * @param path
	 * @param f
	 */
	public Personatge(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.setConstantForce(0, 0.2);
		this.vidamax = 3;
		this.vida = this.vidamax;
		this.energiamax = 10;
		this.energia = this.energiamax;
		this.Br_rec = 0;
		this.DMG = 1;

		for (int i = 0; i < this.vida; i++) {
			Vida vid = new Vida("Vida" + i, 25 + i * 50, 30, 65 + i * 50, 70, angle, "resources/corazon1.gif", f);
			this.vidas.add(vid);
		}
		
		Br_ene = new Vida("Energia", 25, 100, this.energia*10 + 25, 120, 0, Color.cyan, f);
	}
	/**
	 * Constructor por copia de personaje.
	 * @param p
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param f
	 */
	public Personatge(Personatge p, int x1, int y1, int x2, int y2, Field f) {
		super(p.name, x1, y1, x2, y2, 0, p.path, f);
		this.setConstantForce(0, 0.2);
		this.vidamax = p.vidamax;
		this.vida = p.vida;
		this.energiamax = 10;
		this.energia = this.energiamax;
		this.Br_rec = 0;
		
		for (int i = 0; i < this.vida; i++) {
			Vida vid = new Vida("Vida" + i, 25 + i * 50, 30, 65 + i * 50, 70, angle, "resources/corazon1.gif", f);
			this.vidas.add(vid);
		}
		
		Br_ene = new Vida("Energia", 25, 100, this.energia*10 + 25, 120, 0, Color.cyan, f);
	}
	// FUNCIONES

	/**
	 * Funcion que detecta y procesa las colisiones de Personaje.
	 */
	@Override
	public void onCollisionEnter(Sprite sprite) {

		if (this.y2 <= sprite.y1) {
			aterra = true;
			doublejump = 1;
		}

		

		if (sprite instanceof Puerta) {
			Puerta pu = (Puerta) sprite;
			Pantalla1.w.close();
			pu.next(this);

		}
	}

	/**
	 * Funcion que detecta y procesa la salida de colision de Personaje.
	 */
	@Override
	public void onCollisionExit(Sprite sprite) {
		if (this.y2 < sprite.y2) {
			aterra = false;
		}
	}

	/**
	 * Funcion que procesa las colisiones con los trigger.
	 */
	@Override
	public void onTriggerEnter(Sprite sprite) {
		if (sprite instanceof Pocion) {
			Pocion p = (Pocion) sprite;
			p.recoger();
			if (this.vida < this.vidamax) {
				Vida vid = new Vida("Vida", vidas.get(this.vida - 1).x1 + 50, vidas.get(this.vida - 1).y1,
						vidas.get(this.vida - 1).x2 + 50, vidas.get(this.vida - 1).y2, angle, "resources/corazon1.gif",
						f);
				vidas.add(vid);
				this.vida++;
			}
			this.energia = this.energiamax;
			this.Br_ene.x2 = (this.energia*10) + this.Br_ene.x1;
		}
	}
	
	/**
	 * Movimiento del personaje en funcion de la tecla que le llege.
	 * 
	 * @param key
	 */
	public void movimentp(Set<Character> key) {

		if (key.contains('d') || key.contains('D')) {
			this.setVelocity(8, this.velocity[1]);
			flippedX = false;

		} else if (key.contains('a') || key.contains('A')) {
			this.setVelocity(-8, this.velocity[1]);
			flippedX = true;

		} else if (aterra) {
			this.setVelocity(0, this.velocity[1]);
		}

		if (key.contains('4') || key.contains('5') || key.contains('6') || key.contains('8')) {
			PewPew(key);
		}

	}

	

	/**
	 * Funcion que controla el funcionamiento del salto
	 */
	public void saltar() {

		if (aterra == true || doublejump > 0) {
			this.setForce(0, 0);
			this.setVelocity(0, 0);
			this.addForce(0, -1.8);

			doublejump--;
		}
	}

	/**
	 * Funcion que crea el proyectil en la direccion en la que se dirpara.
	 */
	public void PewPew(Set<Character> key) {

		
		if(this.cooldown > 20 && this.energia > 0) {
			
			this.energia--;
			this.Br_ene.x2 = (this.energia*10) + this.Br_ene.x1;
			this.cooldown = 0;
		
			if (key.contains('6') ) {
			this.pew = new Projectil("spit", (this.x1 + 76), this.y1 + 12, (this.x2 + 26), this.y2 - 43, 0,
					"resources/Proyectil1.png", f, this.DMG);
			pew.setVelocity(15, 0);
		} else if(key.contains('4') ){
			this.pew = new Projectil("spit", (this.x1 - 26), this.y1 + 12, (this.x2 - 76), this.y2 - 43, 0,
					"resources/Proyectil1.png", f, this.DMG);
			pew.setVelocity(-15, 0);
			pew.flippedX = true;
		}
		else if(key.contains('8') ){
			this.pew = new Projectil("spit", (this.x1 + 25), this.y1 - 25, (this.x2 - 25), this.y2 - 76, 90,
					"resources/Proyectil1.png", f, this.DMG);
			pew.setVelocity(0, -15);
			pew.flippedX = true;
		}
		else if(key.contains('5') ){
			this.pew = new Projectil("spit", (this.x1 + 25), this.y1 + 77, (this.x2 - 25), this.y2 + 27, 270,
					"resources/Proyectil1.png", f, this.DMG);
			pew.setVelocity(0, 15);
			pew.flippedX = true;
		}
		}
		

	}

	/**
	 * Funcion a la que se llama cada frame.
	 */
	@Override
	public void update() {
		this.cooldown++;
		this.Br_rec++;
		if(this.Br_rec > 150 && this.energia < this.energiamax) {
		this.energia++;
		this.Br_ene.x2 = (this.energia*10) + this.Br_ene.x1;
		this.Br_rec = 0;
		}

	}

	@Override
	public void danyar(int dmg) {

		for (int i = 0; i < dmg; i++) {
			
			if(this.vida > 1) {		
				this.vida--;
				this.vidas.get(this.vida).delete();
				this.vidas.remove(this.vida);
				this.setForce(0, -1);

				}else {
			this.delete();
			Pantalla1.w.close();

		}
		}
		
	}

}
